# See also

## My other projects

[j5t](https://gitlab.com/smalltux/j5t) assumes ES2019+ and converts a JavaScript program into seven unique characters, without using "+" or parenthesis.

[j8t](https://gitlab.com/smalltux/j8t) is designed to support ES5+ and converts a JavaScript program into ten unique characters.

## Projects by other people

This project does not exist in a vacuum - if I hadn't known about [JSFuck](https://github.com/aemkei/jsfuck) and [Hieroglyphy](https://github.com/alcuadrado/hieroglyphy), I likely would never have got here.

# Overview

j4t takes any JavaScript program (with some necessary exceptions, see notes in the bullet points below) and converts it into an equivalent program using only six unique characters - typically "+!\[\]()" - see the "Hello world" image below for an example.

A key feature that distinguishes j4t from many similar programs is that it strictly complies with the ES2015 (ES6) specification. While it also relies on a single guarantee made by the ES2019 (ES10) specification, in many cases it'll also work with engines that are implementing older standards. (When using a JS engine that is compliant with ES2019, j4t will always work.)

Other notable features:

- Any features that are (as of ES2019) deprecated, optional, and / or only available in specific context(s) such as within a web browser, are avoided.
- One of the characters in the output can be chosen by the user - instead of the boolean NOT operator ("!") one can instead choose "=", "<" or ">".
- j4t can convert itself.
- Convert virtually any JavaScript program into six unique characters. Known limitations:
  - Any feature that is not accessible to a function created with the `Function` constructor will not work. For instance:
    - Any reference to `require()` in the encoded program will not work in Node.js since e.g. `Function("require('fs')")()` will crash.
    - Exporting modules is not possible from within a program encoded with j4t.
    - Importing modules requires running the generated code in a JS engine that has the asynchronous `import()` feature available (ES2020+).
  - Very large programs will run slowly, or not at all, due to the resources needed in order to represent and decode the program; expect the encoded program to be on the order of 50 times larger than the original program, plus a fixed amount of 30 kB for the decoding logic (more overhead - both static and dynamic - when using a boolean operator other than "!"). This can partly be worked around by supplying more heap space when running the program.
# Hello world

The following image was generated from the included source file `hello.js`. The same code that is shown in the screenshot can also be found in `hello.js.j4t`.

![Hello world, converted by j4t](hello.png "Hello world, converted by j4t")

Dimensions: 453 x 71 characters (1815 x 571 pixels)

Exact commands used to generate this image:

- `git checkout 4223231`
- `cat hello.js | node j4t.js -op not -inflate -f bmp -ic 453 | convert - hello.png`
- `optipng -strip all -o7 -zm1-9 hello.png`

# Usage

Run `node j4t` for help.

# Thoughts for the future

In theory, some upcoming JavaScript feature could make it possible to use just five unique characters, without compromising on any of the items above.

For instance, if there were one or two methods added to Array.prototype, Number.prototype, and / or String.prototype, with the following conditions, it would be possible to skip the boolean operator entirely:

- Function name consists only of letters present in one of: "Infinity", "NaN" or "undefined"
- Return `true` and / or `false`, perhaps depending on the input parameter

Note that the values `true` and `false` in themselves are not that interesting (they are not even needed for number generation, tricks like `+[]` and `++[[]][+[]] can be used to obtain any number >= 0). It's the letters that can be obtained from them that cannot be obtained elsewhere.
