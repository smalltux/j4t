"use strict";

/*

   This is a proof of concept to show that it's possible - in ES2019 (ES10) or later - to write any JavaScript program
   using just six characters, in a spec-compliant way, without relying on non-core specifications / context-specific
   elements, such as browser DOM.

   For the most readable code, although possibly with known bugs, check the first commit. Optimizations have been done
   over time that make things harder to follow. Note that optimizations are primarily geared towards the default
   boolean operator (i.e. the NOT operator). Further optimization for other modes is possible, but haven't been
   prioritized.

   Naming scheme for variables:
   - No underscore prefix: internal state variables and some special constructs
   - Single underscore prefix: string literals, e.g. "_a" contains code that generates the character "a", "_try"
     generates "try" etc.
   - Double underscore prefix:
   - __str_X generates the sequence of letters hinted at by X; for example "__str_left_parenthesis" generates "(".
   - __obj_X generates a tangible entity hinted at by X; for example "__obj_NaN" is the actual NaN object rather than
     the string "NaN".

   Partial explanations have been added below about how the ES2019 specification makes things work (most of it is
   actually guaranteed by earlier versions, but there are some features used that are guaranteed by ES2019+ only).
   They are *not* meant to imply that one can read one or two passages in the specification and draw conclusions from
   it. Rather, they are provided merely as hints for anyone who is curious and wants to learn roughly how things work
   behind the scenes.

   Any section references are for the ES2019 specification, unless stated otherwise (even for features that are
   present in earlier versions).

   ECMAScript specifications can be found at:
   https://www.ecma-international.org/publications-and-standards/standards/ecma-262/

*/

/* Core helpers for encoding */

function n(i) {
    return try_get_decimal_number(i) || n2(i.toString());
}

function n2(i) {
    let res = "";

    for (let idx = 0; idx < i.length; ++idx) {
        const digit = i[idx];
        let add = "";

        if (digit >= "0" && digit <= "9") {
            add = try_get_decimal_number(digit);
        } else {
            add = [_a, _b, _c, _d, _e, _f][digit.charCodeAt(0) - 97];
        }

        if (idx === 1) {
            add = `+[${add}]`;
        } else if (idx > 1) {
            // Potentially need_wrap could be used to minimize things further. Currently not a useful check though.
            add = `+(${add})`;
        }

        res += add;
    }

    return res;
}

// Example scenario: an expression like ([]+[][[]]) or ([]+[][[]])[+[]] is safe to use for an operation like
// concatenation without wrapping it with parentheses. This function helps to detect some simple such cases.
function need_wrap(expr) {
    const last_char = expr[expr.length - 1];
    if (expr[0] === "(" && (last_char === ")" || last_char === "]")) {
        return false;
    }

    return true;
}

function* encodeString(str, base, pad_to, handle_specials) {
    let first_char_edge_case = str.charCodeAt(0) < 10;

    // For some characters, it is possible to optimize the output size.
    let ch_map = {
        [SPECIAL_TRUE]: `${__obj_true}`,
        [SPECIAL_VAR_NAME_1]: `${__obj_undefined}`,
        [SPECIAL_VAR_NAME_2]: `${__obj_undefined}++`, // Possibly faster than `(${__obj_NaN})`
        "a": _a,
        "d": _d,
        "e": _e,
        "f": _f,
        "i": _i,
        "l": _l,
        "n": _n,
        "r": _r,
        "s": _s,
        "t": _t,
        "u": _u,
        "1": `(${n(1)})`
    };

    let int_map = [];
    for (let i = 0; i <= 127; ++i) {
        const ch = String.fromCharCode(i);
        int_map[i] = ch_map[ch] || "";
    }

    const infix = replaceBlockCommentsAndWhitespace(`+ ${__obj_false} +`);

    let buf = "";
    const BUF_CUTOFF = 4 * 1024; // Reduce the use of slow yields. Somewhat arbitrary cutoff point.

    let n_fn = base === 10 ? try_get_decimal_number : n2;
    let first_non_special = true;
    let first_special = true;
    let code;
    let raw_digits;
    let idx = 0;

    while (true) {
        if (OPT_MODE >= 2015) {
            code = str.codePointAt(idx);
            if (code >= 0x10000) {
                idx += 2;
            } else {
                idx += 1;
            }
        } else {
            code = str.charCodeAt(idx);
            idx += 1;
        }

        if (!(code >= 0)) {
            break;
        }

        if (handle_specials && int_map[code]) {
            if (first_special) {
                buf += int_map[code];
            } else {
                buf += "+" + int_map[code];
            }

            first_special = false;

            // Redundant to check buf length at this point - any overrun is going to be small since
            // user code is not handled here.
            continue;
        }

        if (base === 10) {
            raw_digits = code.toString();
        } else {
            raw_digits = code.toString(base).padStart(pad_to, "0");
        }

        if (handle_specials || !first_non_special) {
            buf += infix;
        }

        let first_digit = true;

        for (const digit of raw_digits) {
            const digit_value = n_fn(digit);

            if (!first_digit) {
                buf += "+";
            }

            if (first_char_edge_case || (first_non_special && first_digit && raw_digits.length > 1)) {
                buf += "[" + digit_value + "]";
            } else if (need_wrap(digit_value)) {
                buf += "(" + digit_value + ")";
            } else {
                buf += digit_value;
            }

            first_digit = false;
        }

        first_non_special = false;
        first_char_edge_case = false;

        if (buf.length > BUF_CUTOFF) {
            yield buf;
            buf = "";
        }
    }

    // Handle any potential leftovers
    yield buf;
}

function* generateUserCodeString(str) {
    yield* encodeString(str, 10, 1, false);
}

function* generateStageString(str) {
    yield* encodeString(replaceBlockCommentsAndWhitespace(str), 16, 2, true);
}

function replaceBlockCommentsAndWhitespace(str) {
    return str
        .replace(/\s+/mg, "")
        .replace(/\/\*space\*\//g, " ")
        .replace(/\/\*linefeed\*\//g, "\n")
        .replace(/\/\*.*?\*\//g, "")
    ;
}

function join(...args) {
    return args.join("+");
}

function* generateJs(user_code) {
    if (!user_code) {
        return;
    }

    const variable_name_1 = SPECIAL_VAR_NAME_1;
    const function_name_1 = SPECIAL_VAR_NAME_2;

    let decoder_fn_name, INVOKER1, INVOKER2;

    if (OPT_MODE >= 2015) {
        decoder_fn_name = "fromCodePoint";
        INVOKER1 = "``";
        INVOKER2 = "``";
    } else {
        decoder_fn_name = "fromCharCode";
        INVOKER1 = '("")';
        INVOKER2 = "()";
    }

    // The stage code can be significantly shortened by using e.g. String.fromCodePoint(...chars) instead of mapping
    // one code point / character at a time and joining them at the end. However, this would quickly consume the
    // stack, which may be undesirable, especially when encoding large programs. The same issue applies when using
    // Function.prototype.apply.
    //
    // "return" is prefixed within decode_expr below
    //
    // Additionally, if OPT_LARGE_INPUT is true, the result from the invoked function is returned so that it can be
    // applied to the actual user code.
    const stage_code_raw = `
        function /*space*/ ${function_name_1}(${variable_name_1}) {
            if (1 ** ${variable_name_1})
                return /*space*/ String.${decoder_fn_name}(${variable_name_1})

            /*linefeed*/

            ${OPT_LARGE_INPUT ? "return /*space*/" : ""} Function(
                ${variable_name_1}
                    .split(!${SPECIAL_TRUE})
                    .map(${function_name_1})
                    .join${INVOKER1}
            )${INVOKER2}
        }
    `;

    let stage_code = "";

    for (const chunk of generateStageString(stage_code_raw)) {
        stage_code += chunk;
    }

    stage_code = replaceBlockCommentsAndWhitespace(stage_code);

    // These temporary markers are replaced in split + join calls below with their respective target strings.

    const mk_lp = __obj_true; // Temporary marker for "("
    let mk_rp; // Temporary marker for ")"

    if (OPT_MODE >= 2015) {
        mk_rp = `(${n(0)})`;
    } else {
        mk_rp = `(${__obj_NaN})`;
    }

    // Temporary marker for "codeURI(undefined" (given that parameter_stage_code indeed produces "undefined")
    const mk_codeURI_etc = `(${n(1)})`;

    // See comment for "exception_variable" for details on creatively named function parameters.
    const parameter_stage_code = __obj_undefined;

    const _de = join(_d,_e);
    const _en = join(_e,_n);
    const _codeURI = join(_c,_o,_d,_e,_U,_R,_I);

    let args_starting_with_percent; // This represents one or more arguments, the first of which is the string "%".

    if (OPT_MODE >= 2022) {
        const _at = join(_a,_t);
        args_starting_with_percent = join(
            _en, mk_codeURI_etc,
            mk_rp,
            __str_period, _at, mk_lp, mk_rp
        );
    } else if (OPT_MODE >= 2015) {
        args_starting_with_percent = join(
            __str_period, __str_period, __str_period,
            _en, mk_codeURI_etc,
            mk_rp
        );
    } else {
        const __str_left_square_bracket = `([] + ${__obj_Array_Iterator_object})[${n(0)}]`;
        const __str_right_square_bracket = `([] + ${__obj_Array_Iterator_object})[${n(22)}]`;
        args_starting_with_percent = join(
            _en, mk_codeURI_etc,
            mk_rp,
            __str_left_square_bracket, `(${n(0)})`, __str_right_square_bracket
        );
    }

    const _function = join(_f,_u,_n,_c,_t,_i,_o,_n);

    // See comments for __str_left_parenthesis and __str_right_parenthesis.
    const __str_left_curly_bracket = `([] + ${__obj_Function}())[${n(22)}]`;
    const __str_right_curly_bracket = `(${__obj_false} + ${__obj_Function}())[${n(30)}]`;
    const _p = `(+(${n(25)}))[${_toString}](${n(30)})`;
    const _split = join(_s,_p,_l,_i,_t);
    const _join = join(_j,_o,_i,_n);

    const decode_body = join(
        _return, mk_lp,
            _function, mk_lp, parameter_stage_code, mk_rp,
            __str_left_curly_bracket,
                _return, mk_lp,
                    _de, mk_codeURI_etc,
                        __str_period, _split, mk_lp,
                            __obj_false,
                        mk_rp,
                        __str_period, _join, mk_lp,
                            args_starting_with_percent,
                        mk_rp,
                    mk_rp,
                mk_rp,
            __str_right_curly_bracket,
        mk_rp
    );

    // mk_codeURI_etc and mk_rp already have parentheses built-in, so those parentheses are omitted below.
    let decode_expr = `
        ${__obj_Function}(
            ${_return} +
            ${__obj_Function}(
                (${decode_body})
                    [${_split}]/*(*/${mk_codeURI_etc}/*)*/[${_join}](${_codeURI} + ${mk_lp} + ${parameter_stage_code})
                    [${_split}]  (  ${mk_lp}           )  [${_join}](${__str_left_parenthesis})
                    [${_split}]/*(*/${mk_rp}         /*)*/[${_join}](${__str_right_parenthesis})
            )()(${__str_space} + ${stage_code})
        )()(X)
    `;

    if (OPT_LARGE_INPUT) {
        decode_expr += "(X)";
    }

    decode_expr = replaceBlockCommentsAndWhitespace(decode_expr);

    const parts = decode_expr.split("X");

    yield parts[0];

    const extra_stage = replaceBlockCommentsAndWhitespace(`
        return /*space*/ _ => Function(decodeURIComponent(
            _
            .replace(/[tf].*?e/g, _ => +(_ > "t"))
            .replace(/.{8}/g, _ => "%" + (+("0b1" + _)).toString(16).slice(1))
        ))()
    `);

    yield* generateUserCodeString(OPT_LARGE_INPUT ? extra_stage : user_code);

    yield parts[1];

    if (OPT_LARGE_INPUT) {
        yield "[]"; // It's better to do [...] + ... in order to save one byte. Skipped for simplicity.

        for (const ch of Buffer.from(user_code)) {
            const bits = ch.toString(2).padStart(8, "0");

            for (const bit of bits) {
                // Ignore compile time and yield like crazy.
                yield (bit === "1") ? "+!![]" : "+![]";
            }
        }

        yield parts[2];
    }
}

/* Helpers for all output modes */

function printProgram(user_code) {
    if (FORMAT === "html") {
        printHtml(user_code);
    } else if (FORMAT === "bmp") {
        printBmp(user_code);
    } else {
        printJs(user_code);
    }
}

function printHtml(user_code) {
    process.stdout.write(
        "<!doctype HTML>"
      + "<html>"
      + "  <head>"
      + "    <title></title>"
      + "    <script>"
    );
    printJs(user_code);
    process.stdout.write(
        "    </script>"
      + "  </head>"
      + "  <body>"
      + "  </body>"
      + "</html>"
    );
}

function printJs(user_code) {
    // process.stdout.cork() doesn't seem to have an effect. Create a local buffer to speed things up.
    // Some very simple logic seems to suffice - simply add things until the buffer exceeds the given soft limit below.
    const size_cutoff = 64 * 1024;
    let buf = "";

    for (const chunk of generateJs(user_code)) {
        buf += chunk;
        if (buf.length >= size_cutoff) {
            process.stdout.write(buf);
            buf = "";
        }
    }

    process.stdout.write(buf); // Write any leftovers
}

function printBmp(user_code) {
    const output = getEntireJsOutput(user_code);
    bmpWrite(output, COLS);
}

function getEntireJsOutput(user_code) {
    let output = "";
    for (const chunk of generateJs(user_code)) {
        output += chunk;
    }
    return output;
}

function bmpWrite(chars, cols) {
    const rows = Math.ceil(chars.length / cols);
    const width = bmpGetImageWidth(cols, true);
    const height = bmpGetImageHeight(rows, true);
    const bmp_row_length = Math.ceil((width * 3) / 4) * 4;
    const bitmap_length = bmp_row_length * height;
    const bmp_h = bmpMakeBmpHeader(bitmap_length, BMP_DIB_HEADER_LENGTH);
    const dib_h = bmpMakeDibHeader(width, height, bitmap_length);
    const bitmap = bmpMakeBitmap(chars, rows, cols);
    const buf = Buffer.from([].concat(bmp_h, dib_h, bitmap));
    process.stdout.write(buf);
}

function bmpGetImageWidth(cols, include_border) {
    return cols * BMP_CHAR_WIDTH + (include_border ? BMP_BORDER_TOTAL_X : 0);
}

function bmpGetImageHeight(rows, include_border) {
    return rows * BMP_CHAR_HEIGHT + (include_border ? BMP_BORDER_TOTAL_Y : 0);
}

function bmpMakeBmpHeader(bitmap_length) {
    const this_header_length = 14;
    return [].concat(
        [0x42, 0x4d], // "BM"
        bmpGetNum(this_header_length + BMP_DIB_HEADER_LENGTH + bitmap_length, 4),
        [0x00, 0x00],
        [0x00, 0x00],
        bmpGetNum(this_header_length + BMP_DIB_HEADER_LENGTH, 4)
    );
}

function bmpMakeDibHeader(width, height, bitmap_length) {
    const this_header_length = BMP_DIB_HEADER_LENGTH;
    const ppm = 2835; // 72 DPI * 39.3701 ~= 2835 pixels per metre
    return [].concat(
        bmpGetNum(this_header_length, 4),
        bmpGetNum(width, 4),
        bmpGetNum(height, 4),
        bmpGetNum(1, 2), // planes
        bmpGetNum(24, 2), // bpp
        bmpGetNum(0, 4), // no compression
        bmpGetNum(bitmap_length, 4),
        bmpGetNum(ppm, 4),
        bmpGetNum(ppm, 4),
        bmpGetNum(0, 4), // 0 colors in palette
        bmpGetNum(0, 4) // 0 (all) important colors
    );
}

function bmpGetNum(num, bytes) {
    let res = Array(bytes).fill(0);
    let idx = 0;
    while (num) {
        res[idx] = num % 256;
        num >>>= 8;
        ++idx;
    }
    return res;
}

function bmpMakeBitmap(chars, rows, cols) {
    let bitmap = [];

    // Bottom border and bottom padding (padding here refers to actual pixels, not the BMP padding)
    for (let x = 0; x < bmpGetImageWidth(cols, true); ++x) {
        bmpPushPixelData(bitmap, BMP_COLOUR_WHITE);
    }
    bmpPadRow(bitmap);
    for (let x = 0; x < bmpGetImageWidth(cols, true); ++x) {
        const is_blue = x >= 1 && x < bmpGetImageWidth(cols, true) - 1;
        bmpPushPixelData(bitmap, is_blue ? BMP_COLOUR_BLUE : BMP_COLOUR_WHITE);
    }
    bmpPadRow(bitmap);

    // Left border, characters, right padding and right border
    const w = bmpGetImageWidth(cols);
    const h = bmpGetImageHeight(rows);
    for (let y = h - 1; y >= 0; --y) {
        // Left border
        bmpPushPixelData(bitmap, BMP_COLOUR_WHITE);

        // Characters
        for (let x = 0; x < w; ++x) {
            const col = x >> 2;
            const row = y >> 3;
            const ch = chars[row * cols + col];
            const char_data = {
                "+": BMP_CHAR_PLUS,
                "!": BMP_CHAR_EXCLAMATION_POINT,
                "=": BMP_CHAR_EQUAL_TO,
                "<": BMP_CHAR_LESS_THAN,
                ">": BMP_CHAR_GREATER_THAN,
                "(": BMP_CHAR_LEFT_PARENTHESIS,
                ")": BMP_CHAR_RIGHT_PARENTHESIS,
                "[": BMP_CHAR_LEFT_SQUARE_BRACKET,
                "]": BMP_CHAR_RIGHT_SQUARE_BRACKET,
            }[ch];
            const char_x = x & 0b11;
            const char_y = y & 0b111;
            if (char_data && char_data[char_y][char_x]) {
                bmpPushPixelData(bitmap, BMP_COLOUR_WHITE);
            } else {
                bmpPushPixelData(bitmap, BMP_COLOUR_BLUE);
            }
        }

        // Right padding and right border
        bmpPushPixelData(bitmap, BMP_COLOUR_BLUE);
        bmpPushPixelData(bitmap, BMP_COLOUR_WHITE);
        bmpPadRow(bitmap);
    }

    // Top border
    for (let x = 0; x < bmpGetImageWidth(cols, true); ++x) {
        bmpPushPixelData(bitmap, BMP_COLOUR_WHITE);
    }
    bmpPadRow(bitmap);

    return bitmap;
}

function bmpPushPixelData(arr, colour) {
    const {r, g, b} = colour;
    arr.push(b, g, r);
}

function bmpPadRow(bitmap) {
    while (bitmap.length % 4) {
        bitmap.push(0);
    }
}

/* Helpers to handle arguments */

function setOp(op) {
    // All the *_try_get_decimal_number functions intentionally make it possible to return undefined.
    // This allows any upstream function to apply its own logic, when there's no "atomic" answer.

    let inc;
    let nums = [];

    const unwrap = str => {
        // For practical reasons, __obj_true and __obj_false may have parentheses that are
        // unnecessary in some cases. For instance, __obj_false may be "([]==[])", but
        // if this is embedded within an array, say, "[([]==[])]", this would be redundant,
        // and "[[]==[]]" would suffice.
        //
        // This function should be called in such situations to remove outer wrapping
        // where possible.

        if (str.startsWith("(") && str.endsWith(")")) {
            return str.substring(1, str.length - 1);
        }
        return str;
    };

    // Normally this kind of trimming happens upstream, but the numbers in these caches
    // will be sent out as-is for performance reasons - so pre-trim them.
    const trim_nums = () => {
        for (let i = 0; i < nums.length; ++i) {
            if (nums[i]) {
                nums[i] = replaceBlockCommentsAndWhitespace(nums[i]);
            }
        }
    };

    const cache_default = inc => {
        nums[0] = "+[]";
        nums[1] = `+ ${__obj_true}`;

        let next_expr = unwrap(__obj_true);
        for (let i = 2; i <= 9; ++i) {
            next_expr = inc(next_expr);
            nums[i] = next_expr;
        }
    };

    const cache_lt = inc => {
        nums[0] = "+[]";
        nums[1] = `++[[]][${nums[0]}]`;
        nums[2] = inc(unwrap(__obj_true));
        nums[3] = inc(nums[2]);
        nums[4] = `${nums[2]} << ${nums[1]}`;
        nums[5] = inc(nums[4]);
        nums[6] = `${nums[3]} << ${nums[1]}`;
        nums[7] = inc(nums[6]);
        nums[8] = `${nums[2]} << ${nums[2]}`;
        nums[9] = inc(nums[8]);
        nums[22] = `${nums[1]} + [${nums[1]}] << ${nums[1]}`;
    };

    const cache_gt = inc => {
        nums[0] = "+[]";
        nums[1] = `++[[]][${nums[0]}]`;
        nums[2] = inc(unwrap(__obj_true));
        nums[3] = inc(nums[2]);
        nums[4] = inc(nums[3]);
        nums[5] = `${nums[1]} + [${nums[0]}] >> ${nums[1]}`;
        nums[6] = inc(nums[5]);
        nums[7] = inc(nums[6]);
        nums[8] = inc(nums[7]);
        nums[9] = `
            ${nums[1]} + [${nums[0]}] + (${nums[0]}) + (${nums[0]}) + (${nums[0]})
            >>
            ${nums[1]} + [${nums[0]}]
        `;
        nums[25] = `${nums[1]} + [${nums[0]}] + (${nums[0]}) >> ${nums[2]}`;
    };

    switch (op) {
        case "not":
            __obj_true = `!![]`;
            __obj_false = `![]`; // See sections 7.1.2 and 12.5.9.1. ([] is an Object.)
            cache_default(expr => `${expr} + ${__obj_true}`);
            break;
        case "eq":
            __obj_true = "([] == +[])"; // Same as "" == "0"
            __obj_false = "([] == [])"; // Short version: they are not the same object.
            cache_default(expr => `++[${expr}][+[]]`);
            break;
        case "lt":
            __obj_true = "([] < +[] + [])"; // Same as "" < "0"
            __obj_false = "([] < [])";
            cache_lt(expr => `++[${expr}][+[]]`);
            break;
        case "gt":
            __obj_true = "(+[] + [] > [])"; // Same as "0" > ""
            __obj_false = "([] > [])";
            cache_gt(expr => `++[${expr}][+[]]`);
            break;
        default:
            return false;
    }

    OP = op;
    trim_nums();
    try_get_decimal_number = digit => nums[digit];

    return true;
}

function* getArgs() {
    const args = process.argv.splice(2);

    for (let i = 0; i < args.length; ++i) {
        switch (args[i]) {
            // Options that take an argument
            case "-o":
            case "-op":
            case "-f":
            case "-ic":
                yield [args[i], args[i + 1]];
                ++i;
                break;
            // Options with no arguments
            default:
                yield [args[i]];
        }
    }
}

function showUsageAndQuit(msg) {
    if (msg) {
        console.error(msg);
        console.error();
    }

    console.error("Usage: node j4t -op MODE [-o OPT] [-h] [-inflate] [-f FORMAT] [-ic IMAGE_COLS]");
    console.error();
    console.error("  -op MODE Select which boolean operator is used in the output:");
    console.error("    not (recommended)");
    console.error("    eq");
    console.error("    lt");
    console.error("    gt");
    console.error();
    console.error("  -o OPT Select the optimization level of the output:");
    console.error("    2015 (default)");
    console.error("    2019");
    console.error("    2022");
    // Possibly ES5 rather than ES5.1 is sufficient as a base. This has not been investigated further.
    console.error("    min (ES5.1 + minimal use of ES2015)");
    console.error("    max (ES2022)");
    console.error();
    console.error("    Note that regardless of optimization level, there is still an indirect dependency");
    console.error("    on a guarantee from the ES2019 specification. See comments in the code.");
    console.error();
    console.error("  -h Attempt to optimize output size for huge input.");
    console.error();
    console.error("  -inflate Increase the output size by a small number of bytes (0 or more).");
    console.error();
    console.error("  -f FORMAT Control the output format:");
    console.error("    js (default)");
    console.error("    bmp (the -ic parameter must be supplied when this is selected)");
    console.error("    html (simple wrapper that contains the JS code surrounded by script tags)");
    console.error();
    console.error("  -ic IMAGE_COLS Control how many columns to use when the output format is bmp.");
    console.error();
    console.error("  Input is taken from stdin.");

    process.exit(1);
}

/* Variables and consts */

const SPECIAL_TRUE = String.fromCodePoint(1);
const SPECIAL_VAR_NAME_1 = String.fromCodePoint(20);
const SPECIAL_VAR_NAME_2 = String.fromCodePoint(21);

let FORMAT = "js";
let OPT_MODE = 2015; // This denotes the target specification level to optimize for.
let INFLATE = false;
let OPT_LARGE_INPUT = false;
let OP;
let __obj_true;
let __obj_false;
let try_get_decimal_number;

const args = process.argv.slice(2);

let arg_names = {};

/* BMP specific variables and consts */

const BMP_DIB_HEADER_LENGTH = 40;
const BMP_BORDER_TOTAL_X = 3; // Top and bottom borders + spacing
const BMP_BORDER_TOTAL_Y = 3; // -||-
const BMP_CHAR_WIDTH = 4;
const BMP_CHAR_HEIGHT = 8;

const BMP_COLOUR_BLUE = {r: 0, g: 0, b: 255};
const BMP_COLOUR_WHITE = {r: 255, g: 255, b: 255};

const BMP_CHAR_PLUS =
    [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,1,0], [0,1,1,1], [0,0,1,0], [0,0,0,0], [0,0,0,0]];
const BMP_CHAR_EXCLAMATION_POINT =
    [[0,0,0,0], [0,0,1,0], [0,0,1,0], [0,0,1,0], [0,0,1,0], [0,0,1,0], [0,0,0,0], [0,0,1,0]];
const BMP_CHAR_EQUAL_TO =
    [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,1,1,1], [0,0,0,0], [0,1,1,1], [0,0,0,0], [0,0,0,0]];
const BMP_CHAR_LESS_THAN =
    [[0,0,0,0], [0,0,0,0], [0,0,0,1], [0,0,1,0], [0,1,0,0], [0,0,1,0], [0,0,0,1], [0,0,0,0]];
const BMP_CHAR_GREATER_THAN =
    [[0,0,0,0], [0,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1], [0,0,1,0], [0,1,0,0], [0,0,0,0]];
const BMP_CHAR_LEFT_PARENTHESIS =
    [[0,0,0,0], [0,0,0,1], [0,0,1,0], [0,1,0,0], [0,1,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]];
const BMP_CHAR_RIGHT_PARENTHESIS =
    [[0,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1], [0,0,0,1], [0,0,0,1], [0,0,1,0], [0,1,0,0]];
const BMP_CHAR_LEFT_SQUARE_BRACKET =
    [[0,0,0,0], [0,1,1,1], [0,1,0,0], [0,1,0,0], [0,1,0,0], [0,1,0,0], [0,1,0,0], [0,1,1,1]];
const BMP_CHAR_RIGHT_SQUARE_BRACKET =
    [[0,0,0,0], [0,1,1,1], [0,0,0,1], [0,0,0,1], [0,0,0,1], [0,0,0,1], [0,0,0,1], [0,1,1,1]];

let COLS = undefined;

/* Read mode selection and setup two of the root objects accordingly */

for (const [name, val] of getArgs()) {
    if (arg_names[name]) {
        showUsageAndQuit(`Unexpected extra parameter: '${name}'`);
    }

    arg_names[name] = true;

    switch (name) {
        case "-o":
            switch (val) {
                case "2015":
                case "2019":
                case "2022":
                    OPT_MODE = Number(val);
                    break;
                case "min":
                    OPT_MODE = 0;
                    break;
                case "max":
                    OPT_MODE = Infinity;
                    break;
                default:
                    showUsageAndQuit(`Invalid optimization type: '${val}'`);
            }
            break;
        case "-op":
            if (!setOp(val)) {
                showUsageAndQuit(`Invalid boolean operator: '${val}'`);
            }
            break;
        case "-h":
            OPT_LARGE_INPUT = true;
            break;
        case "-f":
            switch (val) {
                case "bmp":
                case "html":
                case "js":
                    FORMAT = val;
                    break;
                default:
                    showUsageAndQuit(`Invalid output format: '${val}'`);
            }
            break;
        case "-ic":
            const cols_arg = Number(val);
            if (Number.isInteger(cols_arg) && cols_arg >= 1) {
                COLS = cols_arg;
            } else {
                showUsageAndQuit("Invalid value for -ic parameter.");
            }
            break;
        case "-inflate":
            INFLATE = true;
            break;
        default:
            showUsageAndQuit(`Unexpected parameter: '${name}'`);
    }
}

if (!OP) {
    showUsageAndQuit("No operator selected.");
}

if (FORMAT === "bmp" && COLS === undefined) {
    showUsageAndQuit("-ic is mandatory when format is bmp.");
}

if (FORMAT !== "bmp" && COLS !== undefined) {
    showUsageAndQuit("-ic is only applicable when format is bmp.");
}

const _true = `[] + ${__obj_true}`;
const _false = `[] + ${__obj_false}`;

/* Setup all other objects */

const __obj_undefined = `[][[]]`;
const _undefined = `[] + ${__obj_undefined}`;

// Examples to illustrate what's going on here:
// - Stringifying [1, 2, 3] yields "[1,2,3]"
// - Stringifying [] yields "[]"
// - Stringifying false yields "false"
// - (false + []) yields "false"
// - (1 + [0]) yields "10"
//
// Note that the various expressions that follow are optimized primarily for the boolean NOT operator.

const _a = `(${_false})[${n(1)}]`;
const _d = `(${_undefined})[${n(2)}]`;
const _e = `(${_true})[${n(3)}]`;
const _f = `(${_false})[${n(0)}]`;
const _i = `([${__obj_false}] + ${__obj_undefined})[${n(10)}]`;
const _l = `(${_false})[${n(2)}]`;
const _n = `(${_undefined})[${n(1)}]`;
const _r = `(${_true})[${n(1)}]`;
const _s = `(${_false})[${n(3)}]`;
const _t = `(${_true})[${n(0)}]`;
const _u = `(${_undefined})[${n(0)}]`;

const __obj_Infinity = `+(${n(1)} + ${_e} + (${n(1000)}))`;
const __str_period = `(+(${n(11)} + ${_e} + (${n(20)})) + [])[${n(1)}]`;

const __obj_NaN = `+[${__obj_false}]`;

const _I = `(${__obj_Infinity} + [])[${n(0)}]`;
const _y = `(${__obj_NaN} + [${__obj_Infinity}])[${n(10)}]`;

const _entries = join(_e,_n,_t,_r,_i,_e,_s);

// Turning this object into a string yields "[object Array Iterator]" per sections 19.1.3.6 and 22.1.5.2.2.
// Feature present since ES2015.
const __obj_Array_Iterator_object = `[][${_entries}]()`;

const __str_space = `(${__obj_NaN} + ${__obj_Array_Iterator_object})[${n(10)}]`;
const _b = `([] + ${__obj_Array_Iterator_object})[${n(2)}]`;
const _c = `(${__obj_false} + ${__obj_Array_Iterator_object})[${n(10)}]`;
const _j = `([] + ${__obj_Array_Iterator_object})[${n(3)}]`;
const _o = `([] + ${__obj_Array_Iterator_object})[${n(1)}]`;

const _call = join(_c,_a,_l,_l);
const _constructor = join(_c,_o,_n,_s,_t,_r,_u,_c,_t,_o,_r);

const __str_some_array_function = (_ => {
    switch (OPT_MODE) {
        case 2022: return join(_a,_t);
        case 2019: return join(_f,_l,_a,_t);
        case 2015: return join(_f,_i,_l,_l);
        default: return join(_s,_o,_r,_t);
    }
})();

// This returns an object with which a string of code can be executed.
const __obj_Function = `[][${__str_some_array_function}][${_constructor}]`;

// Thanks to ES2019, curly brackets and parentheses can be located in a spec-compliant way
// when stringifying an anonymous function in this fashion.
//
// Note that chances are that even an engine built to comply with an earlier version would follow
// this pattern for anonymous functions.
//
// The string operated on here is "function anonymous(\n) {\n\n}".
// See section 19.2.1.1.1, item 41, and section 19.2.3.5.

const __str_left_parenthesis = `(${__obj_NaN} + ${__obj_Function}())[${n(21)}]`;
const __str_right_parenthesis = `([] + ${__obj_Function}())[${n(20)}]`;

const _m = `([${n(0)}] + ${__obj_false} + ${__obj_Function}())[${n(20)}]`;
const _name = join(_n,_a,_m,_e);
const __str_empty = `[] + []`;
const _String = `(${__str_empty})[${_constructor}][${_name}]`;
const _toString = join(_t,_o,_String);

// Get U from "[object Undefined]"
const _U = (INFLATE && OP === "not")
    ? `(${n(0)} + [${n(0)}] + ${__obj_Array_Iterator_object}[${_toString}][${_call}]())[${n(10)}]`
    : `(${__obj_NaN} + ${__obj_Array_Iterator_object}[${_toString}][${_call}]())[${n(11)}]`
;

const _try = join(_t,_r,_y);
const _catch = `(+(${n(11408420)}))[${_toString}](${n(31)})`;

// "undefined", or alternatively "NaN", is cheaper to generate than an individual character like "e", and is a
// perfectly valid name to use in this context!
const exception_variable = __obj_undefined;

const try_body = (_ => {
    if (OPT_MODE >= 2015) {
        // Induce a ReferenceError, so it can be turned into a string and the "R" taken out.
        // See section 19.5.3.4 (Error.prototype.toString).
        //
        // ("let" is only a reserved word in strict mode, and it is assumed that it hasn't been defined on the global
        // object. Note that if necessary, it's possible to avoid this assumption by generating a RangeError instead,
        // by invoking e.g. the equivalent of Array(NaN). Another option is to generate a ReferenceError by
        // referencing a variable before a let/const statement.)

        return join(_l,_e,_t);
    } else {
        // Induce a RangeError, so it can be turned into a string and the "R" taken out.
        // See section 19.5.3.4 (Error.prototype.toString).

        const _A = `(${__obj_NaN} + ${__obj_Array_Iterator_object})[${n(11)}]`;
        const _Array = join(_A,_r,_r,_a,_y);

        return `
             ${_Array}
           + ${__str_left_parenthesis} + (${__obj_NaN}) + ${__str_right_parenthesis}
        `;
    }
})();

const _slice = join(_s,_l,_i,_c,_e);

// Extract "\n{\n<try block body>\n}" from
// "function anonymous(\n) {\n<try block body>\n}"

const try_block = `
    ([] + ${__obj_Function}(
        ${try_body}
    ))[${_slice}](${n(21)})
`;

// Extract ")\n{\nreturn <exception variable name>\n}" from
// "function anonymous(\n) {\nreturn <exception variable name>\n}"

const _return = join(_r,_e,_t,_u,_r,_n);
const catch_block_etc = `
    ([] + ${__obj_Function}(
        ${_return} + ${__str_space} + ${exception_variable}
    ))[${_slice}](${n(20)})
`;

const _R = `
    ([] + ${__obj_Function}(
        ${_try}
      + ${try_block}
      + ${_catch} + ${__str_left_parenthesis} + ${exception_variable}
      + ${catch_block_etc}
    )())[${n(0)}]
`;

// Among many others, the following characters are now directly accessible: "c", "d", "e", "n", "o", "I", "R", and "U"
//
// This makes it possible to create "encodeURI" and "decodeURI", which in turn makes it possible to create many other
// characters.
//
// In brief:
// - encodeURI(" ") is "%20"
// - encodeURI(" ")[0] is "%"
// - decodeURI(encodeURI(" ")[0] + 43) is "C"
// - All the characters needed to get access to a function like String.fromCharCode are now directly or indirectly
//   accessible. This makes it possible to produce arbitrary output.

/* Read user input and print out encoded program */

let input_buf = Buffer.from([]);
process.stdin.on("readable", () => {
    const chunk = process.stdin.read();
    if (chunk !== null) {
        input_buf = Buffer.concat([input_buf, chunk]);
    }
});
process.stdin.on("end", () => {
    const user_code = input_buf.toString("utf8");
    printProgram(user_code);
});
